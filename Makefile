CC=gcc
SRC=src
INCLUDE=include
CFLAGS=-I$(INCLUDE) -O3 -lm -lcsfml-graphics \
					-lcsfml-window -lcsfml-audio -lcsfml-system
OUTPUT=chip8
SRCS = \
	$(SRC)/main.c 	 	\
	$(SRC)/chip8.c 	 	\
	$(SRC)/cpu.c 	 	\
	$(SRC)/io.c 	 	\
	$(SRC)/util.c


chip8: $(SRCS)
	$(CC) -Wall -o $(OUTPUT) $(SRCS) $(CFLAGS) 

.PHONY: clean

clean:
	rm -f $(OUTPUT)
