#include <stdlib.h>
#include "chip8.h"

int main(int argc, char** argv){
	//init all sybsystems
	chip8_cpu_init();
	chip8_io_init();
	chip8_util_init();
	
	//load game
	chip8_load_rom(argv[1]);
	
	//start main loop
	chip8_run();

	//clean up the mess
	chip8_shutdown();

	exit(EXIT_SUCCESS);
}
