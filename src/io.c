#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <SFML/Audio.h>
#include <SFML/Window/Keyboard.h>
#include <SFML/Graphics.h>
#include "chip8.h"
#include "cpu.h"
#include "io.h"
#include "util.h"

sfRenderWindow* window;
sfSoundBuffer* buffer;
sfSound* sound;
sfEvent event;

sfSprite* sprite;

int io_video_init(){
  sfVideoMode mode = {640, 320, 32};

  sfImage* image;
  sfTexture* texture;

  image = sfImage_createFromColor(10, 10, sfWhite);
  texture = sfTexture_createFromImage(image, NULL);
  sprite = sfSprite_create();
  sfSprite_setTexture(sprite, texture, false);

  /* Create new windows */
  window = sfRenderWindow_create(mode,
                                 "CHIP8",
                                 sfTitlebar | sfClose, NULL);

  //sfRenderWindow_setFramerateLimit(window, 60);
  if (!window)
    return EXIT_FAILURE;

  io_video_clear();
  return 0;
}

int io_sound_init(){
  buffer = sfSoundBuffer_createFromFile("res/beep.wav");

  sound = sfSound_create();
  sfSound_setBuffer(sound, buffer);
  sfSound_setLoop(sound, 1);

  return 0;
}

int io_keyboard_init(){
  return 0;
}


void io_video_draw(uint8_t x, uint8_t y, uint8_t n){
  int bit, offset = 0;
  uint8_t current;
  /* Update video memory */
  //printf("draw(%d, %d, %d)\n",x,y,n);
  for(offset = 0; offset < n; offset++){
    current = ram_memory[IR + offset];
    //printf("Current: %x\n", current);
    for(bit=0; bit < BYTE_LENGTH; bit++){
      if( ((current >> (BYTE_LENGTH - 1 - bit)) & 0x1) == 1){
        set_pixel((x+bit) % VIDEO_WEIGHT,
                  (y+offset) % VIDEO_HEIGHT);
      }
    }
  }
  /* Draw sprite */
  io_video_render();
}

void io_video_clear(){
  /* Clear video momory */
  memset(video_memory, 0, VIDEO_LENGTH);
  /* Clear the screen */
  sfRenderWindow_clear(window, sfBlack);
  /* Update the window */
  sfRenderWindow_display(window);
}

void io_video_render(){
  sfSprite* temp;
  int x, y;

  /* Clear the screen */
  sfRenderWindow_clear(window, sfBlack);

  for(y = 0; y < VIDEO_HEIGHT; y++){
    for(x = 0; x < VIDEO_WEIGHT; x++){
      if(is_pixel_set(x,y)){
        //printf("Pixel %d:%d is set\n", x,y);
        temp = sfSprite_copy(sprite);
        sfSprite_setPosition(temp, (sfVector2f){x*10, y*10});
        sfRenderWindow_drawSprite(window, temp, NULL);
      }
    }
  }

  /* Update the window */
  sfRenderWindow_display(window);
}

void io_video_reflesh(){
  sfRenderWindow_display(window);
}

bool io_video_running(){
  if (sfRenderWindow_isOpen(window)){
    while (sfRenderWindow_pollEvent(window, &event)){
      /* Close window : exit */
      if (event.type == sfEvtClosed){
        return false;
      }
    }
    return true;
  }
  return false;
}

void io_sound_play(){
  sfSoundStatus status;

  status = sfSound_getStatus(sound);
  if(status == sfPaused || status == sfStopped)
    sfSound_play(sound);
}

void io_sound_stop(){
  sfSound_stop(sound);
}

bool io_key_isPressed(int key){
  key = key_to_map(key);
  return sfKeyboard_isKeyPressed(key);
}

int io_key_get(){
  for(;;){
    if(sfWindow_pollEvent((sfWindow*)window, &event))
      if(event.type == sfEvtKeyPressed){
        if(map_to_key(event.key.code) == -1)
          continue;
        return map_to_key(event.key.code);
      }
    usleep(10);
  }
}


void io_shutdown(){
  /* Cleanup resources */
  sfSound_destroy(sound);
  sfSoundBuffer_destroy(buffer);
  sfRenderWindow_destroy(window);
}
