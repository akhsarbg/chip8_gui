#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "chip8.h"
#include "cpu.h"
#include "io.h"
#include "util.h"

/* 80 bytes */
const uint8_t font[] = {
  0xF0, 0x90, 0x90, 0x90, 0xF0, // '0'
  0x20, 0x60, 0x20, 0x20, 0x70, // '1'
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // '2'
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // '3'
  0x90, 0x90, 0xF0, 0x10, 0x10, // '4'
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // '5'
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // '6'
  0xF0, 0x10, 0x20, 0x40, 0x40, // '7'
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // '8'
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // '9'
  0xF0, 0x90, 0xF0, 0x90, 0x90, // 'A'
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // 'B'
  0xF0, 0x80, 0x80, 0x80, 0xF0, // 'C'
  0xE0, 0x90, 0x90, 0x90, 0xE0, // 'D'
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // 'E'
  0xF0, 0x80, 0xF0, 0x80, 0x80  // 'F'
};

const size_t font_length = sizeof(font) / sizeof(font[0]);
const size_t char_height = 8;
const size_t char_size = 5;

int chip8_io_init(){
  io_video_init();
  io_sound_init();
  io_keyboard_init();

  return 0;
}

void chip8_cpu_init(){
  /* Load font into memory */
  memcpy(ram_memory, font, font_length);

  stack_init();

  /* Reset CPU control registers */
  IR = 0;
  PC = _START;
  delay_timer = 0;
  sound_timer = 0;
}

int chip8_util_init(){
  util_rand_init();
  return 0;
}

int chip8_load_rom(char* path){
  FILE * rom;
  long lSize;
  size_t result;

  rom = fopen(path, "r");
  if(rom == NULL) { exit(EXIT_FAILURE); }

  /* Obtain file size: */
  fseek (rom , 0 , SEEK_END);
  lSize = ftell (rom);
  rewind (rom);

  /* Read game into memory */
  result = fread (ram_memory+_START, 1, lSize, rom);
  if (result != lSize) {fputs ("Reading error",stderr); exit (3);}

  /* Close rom file and return its length */
  fclose (rom);
  return result;
}

void chip8_timers_tick(){
  /* Take care of timers */
  if(delay_timer > 0)
    delay_timer--;
  if(sound_timer > 0){
    io_sound_play();
    sound_timer--;
  }
}

void chip8_run(){
  /* System clock is based on speed mechanism
     which is not exactly reliable, should rewrite
     using timer interrupts
  */
  int x = 0;
  while(io_video_running()){
    /* 60Hz */
    if(x % 33){
      chip8_timers_tick();
    }else{
      io_sound_stop();
    }
    /* 500Hz */
    if(x % 4){
      cpu_tick();
    }
    usleep(1000);
    io_video_reflesh();
    x++;
  }
}


void chip8_shutdown(){
  io_shutdown();
}
