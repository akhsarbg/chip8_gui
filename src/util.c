#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <SFML/Window/Keyboard.h>
#include "util.h"
#include "cpu.h"

char keymap[16][2] = {
	{sfKeyNum1,    1},
	{sfKeyNum2,    2},
	{sfKeyNum3,    3},
	{sfKeyNum4,   0xc},
	{sfKeyQ,       4},
	{sfKeyW,       5},
	{sfKeyE,       6},
	{sfKeyR,      0xd},
	{sfKeyA,       7},
	{sfKeyS,       8},
	{sfKeyD,       9},
	{sfKeyF,      0xe},
	{sfKeyZ,      0xa},
	{sfKeyX,       0},
	{sfKeyC,      0xb},
	{sfKeyV,      0xf},
};

void util_rand_init(){
	srand(time(NULL));   // should only be called once
}

int util_rand(){
	return rand() % 0xFF;;
}

/* 
   Used on machines with more keys that chip8

   Fuction is depricated
 */
bool is_valid_key_code(int key){
	int i;
	for(i=0; i<0xF; i++)
		if(key == keymap[i][0])
			return true;
	return false;
}

void set_pixel(int x, int y){
	uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
	uint16_t offset = x % BYTE_LENGTH;
	uint16_t bit = (BYTE_LENGTH-1) - offset;

	video_memory[index] ^= ( 1 << bit );

	// No need to perform this is V[0xF]
	// is true from earlier
	if(!V[0xF])
		V[0xF] |=  !(video_memory[index] >> bit & 1);
}

bool is_pixel_set(int x, int y){
	uint8_t b = -1;
	uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
	uint16_t offset = x % BYTE_LENGTH;
	uint16_t bit = (BYTE_LENGTH-1) - offset;

	b = (video_memory[index] >> bit) & 1;
	return b;
}

/* Binary-Coded Decimal */
void bcd(int dec){
	int i, sum = 0;

	for(i=0; i<BYTE_LENGTH; i++){
		sum += (dec >> i & 1) * pow(2, i);
	}

	ram_memory[IR+0] = sum % 1000 / 100;
	ram_memory[IR+1] = sum % 100 / 10;
	ram_memory[IR+2] = sum % 10 / 1;
}

int key_to_map(int key){
	int i;
	for(i=0; i<0xF; i++){
		if(key == keymap[i][1]) return keymap[i][0];
	}
	return -1;
}

int map_to_key(int map){
	int i;
	for(i=0; i<0xF; i++){
		if(map == keymap[i][0]) return keymap[i][1];
	}
	return -1;
}

void print_debug_info(){
	int i;
	printf("IR: %X\n", IR);
	printf("PC: %X\n", PC);
	printf("Delay: %X\n", delay_timer);
	printf("Sound: %X\n\n", sound_timer);
	for(i=0; i<0xF; i++){
		printf("V[%x]: %X\n", i, V[i]);
	}
	stack_print();
}

void print_debug_and_exit(){
	print_debug_info();
	exit(1);
}

void hexDump (char *desc, void *addr, int len) {
	int i;
	unsigned char buff[17];
	unsigned char *pc = (unsigned char*)addr;

	// Output description if given.
	if (desc != NULL)
		printf ("%s:\n", desc);

	if (len == 0) {
		printf("  ZERO LENGTH\n");
		return;
	}
	if (len < 0) {
		printf("  NEGATIVE LENGTH: %i\n",len);
		return;
	}

	// Process every byte in the data.
	for (i = 0; i < len; i++) {
		// Multiple of 16 means new line (with line offset).

		if ((i % 16) == 0) {
			// Just don't print ASCII for the zeroth line.
			if (i != 0)
				printf ("  %s\n", buff);

			// Output the offset.
			printf ("  %04x ", i);
		}

		// Now the hex code for the specific character.
		printf (" %02x", pc[i]);

		// And store a printable ASCII character for later.
		if ((pc[i] < 0x20) || (pc[i] > 0x7e))
			buff[i % 16] = '.';
		else
			buff[i % 16] = pc[i];
		buff[(i % 16) + 1] = '\0';
	}

	// Pad out last line if not exactly 16 characters.
	while ((i % 16) != 0) {
		printf ("   ");
		i++;
	}

	// And print the final ASCII bit.
	printf ("  %s\n", buff);
}
