#ifndef CHIP8_GUI_IO_H
#define CHIP8_GUI_IO_H

#include <stdbool.h>

int io_video_init();
int io_sound_init();
int io_keyboard_init();

void io_video_draw(uint8_t, uint8_t, uint8_t);
void io_video_clear();
void io_video_render();
void io_video_reflesh();
bool io_video_running();
	
void io_sound_play();
void io_sound_stop();

bool io_key_isPressed(int key);
int io_key_get();

void io_shutdown();
#endif
